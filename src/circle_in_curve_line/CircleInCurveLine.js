import React, { useEffect, useRef } from "react";
import { ReactComponent as BaseSvg } from "./curveLine.svg";
import Vis from "./Vis";
import * as d3 from "d3";

export default function CircleInCurveLine() {
  const divRef = useRef();

  function draw() {
    const div = d3.select(divRef.current);
    var svg = div.select("svg");

    const vis = new Vis(svg);
    vis.seq();
  }

  useEffect(() => {
    draw();
  }, []);

  return (
    <div ref={divRef}>
      <BaseSvg />
    </div>
  );
}
