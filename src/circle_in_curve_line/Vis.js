import AnimateCircle from "./animateCircle";

export default class Vis {
  constructor(svg) {
    this.svg = svg;
  }
  initiate() {
    console.log("initiate the required things before the main animation");
  }

  display_header() {
    console.log("header will display");
  }

  start_animate(){
    const animateCircle = new AnimateCircle(this.svg);
    animateCircle.seq();
  }

  wrapper(f, t, a) {
    return new Promise((res, rej) => {
      f.call(this, t, a);
      setTimeout(() => res("done"), t);
    });
  }

  async seq() {
    await this.wrapper(this.initiate, 100);
    await this.wrapper(this.display_header, 100);

    await this.wrapper(this.start_animate, 3000, 1);
  }
}
