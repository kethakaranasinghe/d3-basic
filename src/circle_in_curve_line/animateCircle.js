import * as fn from "./simple_functions.js";

export default class AnimateCircle {
  constructor(svg) {
    this.svg = svg;
    // this.g = this.svg.select("#" + "g" + id);
  }

  initiate() {
    console.log("starting single component");
  }

  get_path() {
    this.path = this.svg.select("path");
    console.log(this.path);
  }

  change_path_color() {
    this.path.attr("stroke", "orange");
    //this.path.attr('stroke', 'red')
  }

  get_circle() {
    this.circle = this.svg.select("circle");
    this.circle.attr("fill", "black");
  }

  get_end() {
    this.end = fn.lineGetEnd(this.path);
  }

  get_start() {
    this.start = fn.lineGetStart(this.path);
  }

  move_to_start(t) {
    fn.move_circle_to_point(this.circle, this.start, t);
  }

  glow_up(t) {
    this.circle.transition().duration(t).attr("r", 18);
  }

  move_along_the_path(t) {
    this.circle.call(fn.move_along_path, this.path, t);
    console.log("testing");
  }

  wrapper(f, t, a) {
    return new Promise((res, rej) => {
      f.call(this, t, a);
      setTimeout(() => res("done"), t);
    });
  }

  async seq() {
    await this.wrapper(this.initiate, 100);
    await this.wrapper(this.get_path, 100);
    await this.wrapper(this.change_path_color, 100);
    await this.wrapper(this.get_circle, 900);
    await this.wrapper(this.get_start, 100);
    await this.wrapper(this.get_end, 100);

    await this.wrapper(this.move_to_start, 300);
    await this.wrapper(this.glow_up, 300);
    await this.wrapper(this.move_along_the_path, 1800);
  }
}
