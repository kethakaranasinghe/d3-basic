import * as d3 from "d3";

export function lineGetEnd(ele) {
  const l = ele.node().getTotalLength();
  const point = ele.node().getPointAtLength(l);

  return [point.x, point.y];
}

export function lineGetStart(ele) {
  const point = ele.node().getPointAtLength(0);

  return [point.x, point.y];
}

export function move_circle_to_point(selection, p, t) {
  selection
    .transition()
    .ease(d3.easeLinear)
    .duration(t)
    .attr("cx", p[0])
    .attr("cy", p[1]);
}

export function move_along_path(selection, path, t) {
  selection
    .transition()
    .duration(t)
    .ease(d3.easeLinear)
    .tween("planet_path", () => move_on_path(path));
}

export function move_on_path(p) {
  const l = p.node().getTotalLength();
  const r = d3.interpolate(0, l);

  return function (t) {
    const point = p.node().getPointAtLength(r(t));
    d3.select(this).attr("cx", point.x).attr("cy", point.y);
  };
}
